# Generated by Django 4.0.3 on 2023-06-13 15:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0006_remove_sale_salesperson_automobilevo_import_href_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='automobilevo',
            name='import_href',
            field=models.CharField(max_length=200, unique=True),
        ),
    ]
