# Generated by Django 4.0.3 on 2023-06-13 15:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0005_alter_sale_price'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sale',
            name='salesperson',
        ),
        migrations.AddField(
            model_name='automobilevo',
            name='import_href',
            field=models.CharField(default=1, max_length=200, unique=True),
        ),
        migrations.AddField(
            model_name='sale',
            name='rep',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, related_name='sales', to='sales_rest.salesperson'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='customer',
            name='address',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='customer',
            name='first_name',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='customer',
            name='last_name',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='customer',
            name='phone_number',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='sale',
            name='automobile',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='sales', to='sales_rest.automobilevo'),
        ),
        migrations.AlterField(
            model_name='sale',
            name='customer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='sales', to='sales_rest.customer'),
        ),
        migrations.AlterField(
            model_name='sale',
            name='price',
            field=models.PositiveIntegerField(),
        ),
        migrations.AlterField(
            model_name='salesperson',
            name='employee_id',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='salesperson',
            name='first_name',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='salesperson',
            name='last_name',
            field=models.CharField(max_length=100),
        ),
    ]
