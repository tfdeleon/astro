import React, {useState, useEffect} from 'react'


function TechnicianForm(){
    const[firstName, setFirstName] = useState('');
    const[lastName, setLastName] = useState('');
    const[employeeID, setEmployeeID] = useState('');

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleEmployeeIDChange = (event) => {
        const value = event.target.value;
        setEmployeeID(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeID
        const techUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig={
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(techUrl, fetchConfig);

        if(response.ok){
            setFirstName('');
            setLastName('');
            setEmployeeID('');
        }
    }
    return (
        <div className="container ">
            <div className="my-5">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="card shadow">
                            <div className="card-body">
                                <form onSubmit={handleSubmit} id="create-technician-form">
                                    <h3 className="card-title text-center">Add a Technician</h3>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={handleFirstNameChange} required placeholder="First name" type="text" id="firstName" name="firstName" className="form-control" value={firstName}/>
                                                <label htmlFor="firstName">First Name</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={handleLastNameChange} required placeholder="Last name" type="text" id="lastName" name="lastName" className="form-control" value={lastName}/>
                                                <label htmlFor="lastName">Last Name</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={handleEmployeeIDChange} required placeholder="Employee ID" type="text" id="employeeID" name="employeeID" className="form-control" value={employeeID}/>
                                                <label htmlFor="employeeID">Employee ID</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="text-center">
                                        <button className="btn btn-outline-primary btn-lg btn-block">Create</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default TechnicianForm;
