import React, {useState, useEffect} from 'react'


async function loadData(){
    const response = await fetch('http://localhost:8080/api/appointments/');
    if(response.ok){
        const apptData = await response.json();
        apptData.appointments = apptData.appointments.filter(appt => appt.status === 'CREATED');
        return apptData;
    } else {
        console.error('Failed to fetch appointment data');
        return null;
    }
}


async function checkVIPStatus(appt) {
    const response = await fetch('http://localhost:8100/api/automobiles/');
    if (response.ok) {
        const autoData = await response.json();
        const vipAppts = autoData.autos.filter((auto) =>
            auto.vin === appt.vin && auto.sold
        );
        appt.is_vip = vipAppts.length > 0;
    } else {
        console.error('Failed to fetch auto data');
    }
    return appt;
}


function AppointmentList(){
    const [apptData, setApptData] = useState(null);
    const [refresh, setRefresh] = useState(false);

    useEffect(() => {
        loadData().then(async (data) => {
            if (data.appointments) {
                const appointments = await Promise.all(
                    data.appointments.map(checkVIPStatus)
                );
            setApptData({appointments });
            }
        });
    }, [refresh]);

    if(!apptData || !apptData.appointments){
        return null;
    }

    async function cancel(id){
        try{
            const url = `http://localhost:8080/api/appointments/${id}/cancel/`
            const fetchConfig = {
                method: 'put'
            };
            const response = await fetch(url, fetchConfig)
            setRefresh(!refresh);
        }catch (e) {

        }
    }

    async function complete(id){
        try{
            const url = `http://localhost:8080/api/appointments/${id}/finish/`
            const fetchConfig = {
                method: 'put'
            }
            const response = await fetch(url, fetchConfig)
            setRefresh(!refresh);
        }catch (e){

        }
    }

    return(
        <div className="container">
            <div className="my-5 card">
            <h3>Appointments</h3>
                <table className="table table-striped">
                    <thead className="bg-dark text-light">
                        <tr>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">VIN</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">VIP</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Customer</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Date</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Time</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Technician</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Reason</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {apptData.appointments.map((appt) => {
                            const dateTime = new Date(appt.date_time);
                            const date = dateTime.toLocaleDateString('en-US', {
                                month: '2-digit',
                                day: '2-digit',
                                year: 'numeric'
                            });
                            const time = dateTime.toLocaleTimeString('en-US', {
                                hour: '2-digit',
                                minute: '2-digit',
                                hour12: true
                            })
                            return (
                                <tr key={appt.id}>
                                    <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{appt.vin}</td>
                                    <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{String(appt.is_vip).charAt(0).toUpperCase() + String(appt.is_vip).slice(1)}</td>
                                    <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{appt.customer}</td>
                                    <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{date}</td>
                                    <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{time}</td>
                                    <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{appt.technician.first_name} {appt.technician.last_name}</td>
                                    <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{appt.reason}</td>
                                    <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                                        <button className="btn btn-danger" onClick={() => {cancel(appt.id)}}>
                                            Cancel
                                        </button>
                                    </td>
                                    <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                                        <button className="btn btn-success" onClick={() => {complete(appt.id)}}>
                                            Complete
                                        </button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )

}

export default AppointmentList;
