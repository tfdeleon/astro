import React, {useState, useEffect} from 'react'


async function loadData(){
    const response = await fetch('http://localhost:8080/api/technicians/');
    if(response.ok){
        const techData = await response.json();
        return techData;
    } else {
        console.error('Failed to fetch technician data');
        return null;
    }
}


function TechnicianList(){
    const [techData, setTechData] = useState([]);

    useEffect(() => {
        loadData().then(data => {
            setTechData(data.technicians);
        });
    }, []);

    return (
        <div className="container">
            <div className="my-5 card">
            <h3>Technicians</h3>
                <table className="table table-striped">
                    <thead className="bg-dark text-light">
                        <tr>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Employee ID</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">First Name</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Last Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {techData.map((tech) => (
                            <tr key={tech.employee_id}>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{tech.employee_id}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{tech.first_name}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{tech.last_name}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default TechnicianList;
