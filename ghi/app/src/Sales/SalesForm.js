import {useEffect, useState} from 'react';

function SalesForm() {

    const [formData, setData] = useState({
        automobiles: [],
        automobile: "",
        reps: [],
        rep: "",
        customers: [],
        customer: "",
        price: "",
    });

    // Set default form image
    const [imgUrl, setImg] = useState('https://source.unsplash.com/350x350/?car')

    const fetchData = async () => {
        try {
                const autoUrl = "http://localhost:8100/api/automobiles/";
                const repUrl = "http://localhost:8090/api/salespeople/";
                const customerUrl = "http://localhost:8090/api/customers/";

                const autoResponse = await fetch(autoUrl);
                const repResponse = await fetch(repUrl);
                const customerResponse = await fetch(customerUrl);

                if(!autoResponse.ok) {
                    throw new Error('Request for automobile failed.')
                } else if(!repResponse.ok) {
                    throw new Error('Request for sales rep failed.')
                } else if(!customerResponse.ok) {
                    throw new Error('Request for customer failed.')
                }

                const newAuto = await autoResponse.json();
                const newRep = await repResponse.json();
                const newCustomer = await customerResponse.json();

                // Filter and store unsold automobiles in autosInStock
                const autosInStock = newAuto.autos.filter(
                    (auto) => auto.sold === false
                    );

                    setData((prevData) => ({
                        ...prevData,
                        automobiles: autosInStock,
                        reps: newRep.reps,
                        customers: newCustomer.customers,
                        price: "",
                    }));


                    } catch (error) {
                        console.error(error);
                    }
                }

    useEffect(() => {
        fetchData();
    }, []);

    const handleChange = (event) => {
        const { name, value } = event.target;

        // Change form picture on automobile select
        if(name==="automobile") {
            const autoSelected = formData.automobiles.find(auto => auto.vin===value);
            if(autoSelected) {
                setImg(autoSelected.model.picture_url);
            }
        }

        setData(oldData => ({
            ...oldData,
            [name]: value,
        }));
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...formData};
        delete data.automobiles;
        delete data.reps;
        delete data.customers;

        try {
            const salesUrl = "http://localhost:8090/api/sales/";
            const fetchConfig = {
                method: 'post',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

            const salesResponse = await fetch(salesUrl, fetchConfig)

            if(!salesResponse.ok) {
                throw new Error('Request for sales record failed.')
            }

            // Update "sold" property of selected automobile to true
            const soldVin = formData.automobile;
            const soldAutoUrl = `http://localhost:8100/api/automobiles/${soldVin}/`;
            const soldAutoConfig = {
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({sold: true}),
            };
            const soldAutoResponse = await fetch(soldAutoUrl, soldAutoConfig);

            if(!soldAutoResponse.ok) {
                throw new Error('Sold status update failed...')
            }
            console.log("Sold status update was successful!", soldAutoResponse);
            window.location.reload();


            const autoSold = formData.automobiles.map(auto => {
                if(auto.vin === formData.automobile) {
                    return { ...auto, sold: true };
                }
                return auto;
            });

            setData((prevData) => ({
                ...prevData,
                automobile: "",
                price: "",
                automobiles: autoSold,
            }));

        } catch (error) {
            console.error(error);
        }
    };

    return (
        <div className="row mt-5">
        <div className="col col-sm-auto">
            <img width="350" className="bg-white rounded shadow d-block mx-auto mb-2 pt-4" src={imgUrl} alt="Banner" />
        </div>

        <div className="offset-0 col-8">
            <h2 className="text-center pb-2">Record new sale</h2>
            <form onSubmit={handleSubmit} id="create-sale-form">
                <div className="mb-3">
                    <select value={formData.automobile} onChange={handleChange} required name="automobile" id="automobile" className="form-select">
                        <option value="">Select automobile</option>
                        {formData.automobiles.map(automobile => {
                        return (
                            <option key={automobile.href} value={automobile.vin}>
                            {automobile.year} {automobile.model.manufacturer.name} {automobile.model.name} ({automobile.color})
                            </option>
                        );
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <select value={formData.rep} onChange={handleChange} required name="rep" id="rep" className="form-select">
                        <option value="">Select sales rep</option>
                        {formData.reps.map(rep => {
                        return (
                            <option key={rep.id} value={rep.employee_id}>
                            {rep.first_name} {rep.last_name} - {rep.employee_id}
                            </option>
                        );
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <select value={formData.customer} onChange={handleChange} required name="customer" id="customer" className="form-select">
                        <option value="">Select customer</option>
                        {formData.customers.map(customer => {
                        return (
                            <option key={customer.id} value={customer.id}>
                            {customer.first_name} {customer.last_name} - {customer.phone_number}
                            </option>
                        );
                        })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.price} onChange={handleChange} placeholder="price" type="text" name="price" id="price" className="form-control" />
                    <label htmlFor="price">Price</label>
                </div>
                <div id="button-pull">
                <button className="btn btn-primary btn-block">Create</button>
                </div>
            </form>
            </div>
        </div>
    );
}

export default SalesForm;
