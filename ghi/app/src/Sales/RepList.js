import React, {useEffect, useState} from 'react';

function RepList() {
    const [reps, setReps] = useState([]);
    const fetchData = async () => {
        try {
            const url = 'http://localhost:8090/api/salespeople';
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setReps(data.reps);
            }
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <h2 className="text-center">Sales Representatives</h2>
        <table className="table-fill">
        <thead>
        <tr>
            <th className="text-left">Employee ID</th>
            <th className="text-left">First Name</th>
            <th className="text-left">Last Name</th>
        </tr>
        </thead>
        <tbody className="table-hover">
            {reps.map(rep => {
                return (
                <tr key={ rep.id }>
                    <td className="text-left">{ rep.employee_id }</td>
                    <td className="text-left">{ rep.first_name }</td>
                    <td className="text-left">{ rep.last_name }</td>
                </tr>
                );
            })}
        </tbody>
    </table>
    </>
    );
}

export default RepList;
