import React, {useEffect, useState} from 'react';

function SalesList() {
    const [sales, setSales] = useState([]);
    const [autos, setAutos] = useState([]);

    const fetchData = async () => {
        try {
            const salesUrl = 'http://localhost:8090/api/sales/';
            const autosUrl = 'http://localhost:8100/api/automobiles/';
            const salesResponse = await fetch(salesUrl);
            const autosResponse = await fetch(autosUrl);

            if (!salesResponse.ok) {
                    throw new Error('Request for sales has failed.');
                } else if (!autosResponse.ok) {
                    throw new Error('Request for automobiles has failed.');
                }
                const salesData = await salesResponse.json();
                const autosData = await autosResponse.json();


            // Filtering out unsold cars:
                // Create new filtered array [soldAutos] containing cars marked sold ("sold": true)
                const soldAutos = autosData.autos.filter(auto => auto.sold);
                // Cars in [sales] that have matching vins to filtered [soldAutos] are included in array
                setSales(salesData.sales.filter(sale => soldAutos.some(auto => auto.vin === sale.automobile)));
                // Set [autos] to [soldAutos]
                setAutos(soldAutos);

            } catch (error) {
                console.error(error);
            }
        };

    useEffect(() => {
        fetchData();
    }, []);

    const autosVin = autos.reduce((map, auto) => {
        map[auto.vin] = auto;
        return map;
    }, {});

    // Delineates large numbers by adding commas
    const numberForm = (val) => {
        return val.toLocaleString();
    };

    return (
        <>
            <h2 className="text-center">Sales History</h2>
        <table className="table-fill">
        <thead>
        <tr>
            <th className="text-left">Sales Rep</th>
            <th className="text-left">Customer Contact</th>
            <th className="text-left">Vehicle ID No.</th>
            <th className="text-left">Vehicle</th>
            <th className="text-left">Sale Price</th>
        </tr>
        </thead>
        <tbody className="table-hover">
            {sales.map(sale => {
                const automobile = autosVin[sale.automobile];

                return (
                <tr key={ sale.id }>
                    <td className="text-left">{ sale.rep }</td>
                    <td className="text-left">{ sale.customer }</td>
                    <td className="text-left">{ sale.automobile }</td>
                    <td className="text-left">{automobile ? `${automobile.year} ${automobile.model.manufacturer.name} ${automobile.model.name}` : 'N/A'}</td>
                    <td className="text-left">${numberForm(sale.price)}.00</td>
                </tr>
                );
            })}
        </tbody>
    </table>
    </>
    );
}

export default SalesList;
