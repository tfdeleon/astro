import { CardContent, Typography } from '@mui/material';
import React, {useState, useEffect} from 'react'


async function loadData(){
    const response = await fetch('http://localhost:8100/api/automobiles/');
    if(response.ok){
        const modelData = await response.json();
        return modelData;
    } else {
        console.error('Failed to fetch automobile data');
        return null;
    }
}

// function AutoColumn(props){
//     return (
//         <div className='col'>
//             {autoData.map((auto) =>(
//                 <Card variant="outlined" sx={{width:320}}>
//                     <div>
//                         <Typography level="h2" fonstSize="md" sx={{ md: 0.5 }}>{auto.year} {auto.model.manufacturer.name} {auto.model.name}</Typography>
//                         <AspectRatio minHeight="120px" maxHeight="200px">
//                             <img
//                             src={auto.model.picture_url}
//                             loading="laxy"
//                             alt=""
//                             />
//                         </AspectRatio>
//                         <CardContent orentation="horizontal">
//                             <div>
//                                 <Typography level="body3">Price:</Typography>
//                                 <Typography fontSize="lg" fontWeight="lg">
//                                     $auto.price
//                                 </Typography>
//                             </div>
//                         </CardContent>
//                     </div>
//                 </Card>
//             ))}
//         </div>
//     )
// }


function AutomobileList(){
    const[autoData, setAutoData] = useState([]);
    const[autoColumns, setAutoColumns] = useState([[],[],[]]);
    const columns = [[],[],[]];

    // let i = 0;
    // for(const auto of autoData){
    //     columns[i].push(auto);
    //     i++;
    //     if(i>2){
    //         i=0;
    //     }
    // }
    // setAutoColumns(columns)

    useEffect(()=>{
        loadData().then(data => {
            setAutoData(data.autos);
        })
    }, []);

    return (
        <div className="container">
                    {/* {autoColumns.map((auto) =>(
                        <Card variant="outlined" sx={{width:320}}>
                            <div>
                                <Typography level="h2" fonstSize="md" sx={{ md: 0.5 }}>{auto.year} {auto.model.manufacturer.name} {auto.model.name}</Typography>
                                <AspectRatio minHeight="120px" maxHeight="200px">
                                    <img
                                    src={auto.model.picture_url}
                                    loading="laxy"
                                    alt=""
                                    />
                                </AspectRatio>
                                <CardContent orentation="horizontal">
                                    <div>
                                        <Typography level="body3">Price:</Typography>
                                        <Typography fontSize="lg" fontWeight="lg">
                                            $auto.price
                                        </Typography>
                                    </div>
                                </CardContent>
                            </div>
                        </Card>
                    ))} */}
            <div className="my-5 card">
            <h3>Automobiles</h3>
                <table className="table table-striped">
                    <thead className="bg-dark text-light">
                        <tr>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">VIN</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Color</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Year</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Model</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Manufacturer</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Sold</th>
                        </tr>
                    </thead>
                    <tbody>
                        {autoData.map((auto) => (
                            <tr key={auto.id}>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{auto.vin}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{auto.color}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{auto.year}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{auto.model.name}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{auto.model.manufacturer.name}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{auto.sold ? 'Yes' : 'No'}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default AutomobileList;
