import React, {useState, useEffect} from 'react'

async function loadData() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const data = await response.json();
            return data.manufacturers;
        } else {
            console.error('Failed to fetch manufacturer data');
            return [];
        }
}

    function ManufacturerList() {
        const [manufacturerData, setManufacturerData] = useState([]);

        useEffect(() => {
        loadData().then(data => {
            setManufacturerData(data);
        });
        }, []);

        return (
        <div className="container">
            <div className="my-5 card">
            <h3>Manufacturers</h3>
            <table className="table table-striped">
                <thead className="bg-dark text-light">
                <tr>
                    <th style={{ textAlign: 'left', verticalAlign: 'middle' }} scope="col">Name</th>
                </tr>
                </thead>
                <tbody>
                {manufacturerData.map((manufacturer) => (
                    <tr key={manufacturer.id}>
                    <td style={{ textAlign: 'left', verticalAlign: 'middle' }}>{manufacturer.name}</td>
                    </tr>
                ))}
                </tbody>
            </table>
            </div>
        </div>
        );
    }

    export default ManufacturerList;
