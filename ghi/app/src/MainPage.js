import { Typography } from '@mui/material';
import purple from '@mui/material/colors/purple';
function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <Typography color="secondary" variant="h1">Carverse</Typography>
      {/* <h1 className="display-5 fw-bold">Carverse</h1> */}
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
      </div>
    </div>
  );
}

export default MainPage;
// sx={{ color: purple[500] }}
