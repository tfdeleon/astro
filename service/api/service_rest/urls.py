from django.urls import path
from .views import (
    api_cancel_appointment, api_complete_appointment, api_detail_appointment,
    api_detail_technicians, api_list_appointments, api_list_technicians
)

urlpatterns = [
    path(
        "technicians/",
        api_list_technicians,
        name="api_list_technicians"
        ),
    path(
        "technicians/<int:id>/",
        api_detail_technicians,
        name="api_hat_detail"
        ),
    path(
        "appointments/",
        api_list_appointments,
        name='api_list_appointments'
        ),
    path(
        "appointments/<int:id>/",
        api_detail_appointment,
        name='api_detail_appointment'
        ),
    path(
        "appointments/<int:id>/cancel/",
        api_cancel_appointment,
        name='api_cancel_appointment'
        ),
    path(
        "appointments/<int:id>/finish/",
        api_complete_appointment,
        name='api_complete_appointment'
        )
]
