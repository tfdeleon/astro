from django.contrib import admin
from .models import AutomobileVO, Appointment


@admin.register(AutomobileVO)
class AutomovileVOAdmin(admin.ModelAdmin):
    pass


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    pass


@admin.register
class TechnicianAdmin(admin.ModelAdmin):
    pass
